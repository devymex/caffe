
#ifndef CVHELPER_HPP_
#define CVHELPER_HPP_

#include <opencv2/opencv.hpp>

namespace caffe {

template <typename Dtype>
inline cv::Point_<Dtype> MiddelPoint(
		const cv::Point_<Dtype> &p1,
		const cv::Point_<Dtype> &p2
		)
{
	return (p1 + p2) / 2;
}

template <typename Dtype>
inline cv::Rect_<Dtype> RectOfCenter(
		const cv::Point_<Dtype> &center,
		const cv::Size_<Dtype> &size
		)
{
	return cv::Rect_<Dtype>(
			center.x - size.width / 2,
			center.y - size.height / 2,
			size.width,
			size.height
			);
}

template <typename Dtype>
inline cv::Point_<Dtype> CenterOfSize(const cv::Size_<Dtype> &rectSize)
{
	return cv::Point_<Dtype>(rectSize.width / 2, rectSize.height / 2);
}

} // namespace caffe

#endif /* CVHELPER_HPP_ */
