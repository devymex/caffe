
//*****************************************************************************
// The greatest code is likes a poem that any illustration shall profanes it.
//													-- yumengwang@deepglint.com
//*****************************************************************************


#ifndef FACES_DATA_LAYER_HPP_
#define FACES_DATA_LAYER_HPP_

#include "caffe/layers/data_layer.hpp"

#include <opencv2/opencv.hpp>

#include <condition_variable>
#include <mutex>
#include <random>
#include <string>
#include <vector>

//=============================================================================
// THE LODING PROCEDURE
//1. SetupLayer:
//	1.1. Load all filenames and corresponding landmarks with id
//	1.2. Load all pairs (just stores the id of image)
//2. LoadBatch:
//	2.1. Retrive a pair of the batch from all pairs, and for the two images:
//		2.1.1. Do data augmentation
//		2.1.2. Extract parts of face
//		2.1.3. Do resizing and (random)cropping of the parts to make it a patch
//	2.2. Add app patches from two image into one vector.
//3. MakeBlob:
//	3.1. Convert patch images to float
//	3.2. Fill the buffer of top Blob with patches and label
//=============================================================================

namespace caffe {

typedef std::vector<cv::Point2f> LANDMARK;

	
typedef struct
{
	std::vector<std::string>	imgFns;
	std::vector<int>			labels;
	std::vector<LANDMARK>		landmarks;
	int							nCursor;
	std::mutex					cvMutex;
	std::condition_variable		condVar;
} FACEDATA;

template <typename Dtype>
class FaceDataLayer : public caffe::BasePrefetchingDataLayer<Dtype>
{
public: //--------TYPE DEFINITIONS--------//
	enum FACE_PART
	{
		FP_FULL_FACE	= 0,
		FP_LEFT_EYE		= 1,
		FP_RIGHT_EYE	= 2,
		FP_NOSE			= 3,
		FP_MOUTH		= 4,
		FP_MIDDLE_EYE	= 5,
		FP_LEFT_MOUTH	= 6, // How many fucking mouths does one man has?
		FP_RIGHT_MOUTH	= 7, // Bai Kou Mo Bian
		FP_RIMLESS_FACE	= 8,
		FP_LEFT_BROW	= 9,
		FP_RIGHT_BROW	= 10,
		FP_MIDDLE_BROW	= 11,
		FP_GUARD		= 12, // ‡THIS IS A BOUNDARY GUARD‡
	};

	enum AUG_METHOD
	{
		AM_ILLUMIOUS	= 0,
		AM_GAUSS_BLUR	= 1,
		AM_MOTION_BLUR	= 2,
		AM_JPEG_COMP	= 3,
		AM_RESOLUTION	= 4,
		AM_HSV_ADJ		= 5,
		AM_GUARD		= 6, // ‡THIS IS A BOUNDARY GUARD‡
	};

	enum SHUFFLE_METHOD
	{
		SM_NOCHANGE		= 0,
		SM_ONEBYONE		= 1,
		SM_TWOBYTWO		= 2,
		SM_GUARD		= 3 // ‡THIS IS A BOUNDARY GUARD‡
	};

	enum CONST_UINT_MISCELLANEOUS
	{
		IMAGE_CHANNELS		= 3,
		LANDMARK_LENGTH		= 72,
	};

public: //--------CTOR & DTOR--------//
	// Default constructor and destructor
	explicit FaceDataLayer(const LayerParameter& param);
	virtual ~FaceDataLayer();

public: //--------CAFFE INTERFACES--------//
	// Be called only once when the network initializing
	//	[DataLayerSetUp]
	//		├[_LoadLandmarks]
	//		└[_LoadPairs]
	virtual void DataLayerSetUp(
			const std::vector<Blob<Dtype>*>& bottom,
			const std::vector<Blob<Dtype>*>& top
			);
	virtual void Reshape(
			const vector<Blob<Dtype>*>& bottom,
    		const vector<Blob<Dtype>*>& top
    		);
	// Be called on every forwardings
	//	[load_batch]
	//		├[_LoadPatches]
	//		│	├[_ImageAugmentation]
	//		│	└[_GetFacePartRect]
	//		└[_PatchesToBuffer]
	virtual void load_batch(Batch<Dtype>* pBatch);

protected: //--------WORKING FUNCTIONS--------//
	// Check layer params and load internal used variables
	virtual void _CheckAndLoadParams();

	// Check layer params and load internal used variables
	virtual void _LoadData(FACEDATA &data) const;
	
	// Loads m_Landmarks and m_ImgFns from the specified file of landmarks
	virtual void _LoadLandmarks(
			const std::string &strLmFn,
			std::vector<std::string> &imgFns,
			std::vector<int> &labels,
			std::vector<LANDMARK> &landmarks
			) const;

	// Loads an image by specified id and extract patches from the image
	virtual void _LoadPatches(
			const FACEDATA &data,
			int nImgId,
			std::vector<cv::Mat> &patches
			) const;
	
	virtual Dtype _LoadLabel(const FACEDATA &data, int nImgId);

	// Do some image augmentations such as guassianblu, etc.
	virtual bool _ImageAugmentation(cv::Mat &img, AUG_METHOD augMethod) const;

	// Calucate the rect of sepcified facepart with the size of image and landmarks
	virtual cv::Rect2f _GetFacePartRect(
			const cv::Size &imgSize,
			const LANDMARK &lm,
			FACE_PART facePart
			) const;

protected: //--------PAIRWISEDATA MEMBERS--------//
	// All image filenames found in landmark file
	static FACEDATA		*m_pTrainData;
	static FACEDATA		*m_pTestData;

	// Pseudo-random engine
	mutable std::mt19937	m_rg;

protected: //--------INTERNAL USAGE--------//
	std::string				m_strImgPath;
	uint32_t				m_nTrainCnt;
	uint32_t				m_nTestCnt;

	cv::Size2f				m_FaceFullSize;
	cv::Size2f				m_FaceRimlessSize;
	cv::Size2f				m_FacePartSize;

	cv::Size2f				m_PatchFullSize;
	cv::Size2f				m_PatchCropSize;
	
	FaceDataParameter		m_Params;

public: //--------UTILITIES--------//
	void _PatchesToBatchBuffer(
		const cv::Mat &patch,
		Dtype *pBuf,
		uint32_t nBufElems
		) const;
};

} // namespace caffe

//	┌─┐	┌┬┐	│├┼┤ Funny huh?
//	└─┘	└┴┘	├┼┤│ Hmm...

#endif /* PAIRWISE_FACES_DATA_LAYER_HPP_ */
