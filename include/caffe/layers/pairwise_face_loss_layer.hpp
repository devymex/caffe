#ifndef CAFFE_PAIRWISE_FACE_LOSS_LAYER_HPP_
#define CAFFE_PAIRWISE_FACE_LOSS_LAYER_HPP_

#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"
#include "caffe/layers/loss_layer.hpp"

#include <vector>

namespace caffe
{

template<typename Dtype>
class PairwiseFaceLossLayer: public LossLayer<Dtype>
{
public:
	explicit PairwiseFaceLossLayer(const LayerParameter& param);

	virtual inline const char* type() const
	{
		return "PairwiseFaceLoss";
	}
	
	virtual inline int ExactNumBottomBlobs() const
	{
		return 2;
	}
	
	virtual inline int ExactNumTopBlobs() const
	{
		return 2;
	}
	
protected:
	virtual void LayerSetUp(
			const vector<Blob<Dtype>*>& bottom,
			const vector<Blob<Dtype>*>& top
			);

	virtual void Reshape(
			const vector<Blob<Dtype>*>& bottom,
			const vector<Blob<Dtype>*>& top
			);
			
	virtual void Forward_cpu(
			const vector<Blob<Dtype>*>& bottom,
			const vector<Blob<Dtype>*>& top
			);
			
	virtual void Backward_cpu(
			const vector<Blob<Dtype>*>& top,
			const vector<bool>& propagate_down,
			const vector<Blob<Dtype>*>& bottom
			);

protected:
	PairwiseFaceLossParameter m_Params;
};

}
#endif

