
//*****************************************************************************
// The greatest code is likes a poem that any illustration shall profanes it.
//													-- yumengwang@deepglint.com
//*****************************************************************************


#ifndef PAIRWISE_FACES_DATA_LAYER_HPP_
#define PAIRWISE_FACES_DATA_LAYER_HPP_

#include "caffe/layers/face_data_layer.hpp"
#include "caffe/layers/data_layer.hpp"

#include <opencv2/opencv.hpp>

#include <condition_variable>
#include <mutex>
#include <random>
#include <string>
#include <vector>

//=============================================================================
// THE LODING PROCEDURE
//1. SetupLayer:
//	1.1. Load all filenames and corresponding landmarks with id
//	1.2. Load all pairs (just stores the id of image)
//2. LoadBatch:
//	2.1. Retrive a pair of the batch from all pairs, and for the two images:
//		2.1.1. Do data augmentation
//		2.1.2. Extract parts of face
//		2.1.3. Do resizing and (random)cropping of the parts to make it a patch
//	2.2. Add app patches from two image into one vector.
//3. MakeBlob:
//	3.1. Convert patch images to float
//	3.2. Fill the buffer of top Blob with patches and label
//=============================================================================

namespace caffe {

// Make sure the size of IMGPAIR is 12 bytes
typedef struct __attribute__((packed, aligned(4)))
{
	int imgId1;	// The Id is the index of m_ImgFns or m_Landmarks
	int imgId2;
	int label;	// 0 for different and 1 for identical
} IMGPAIR;

template <typename Dtype>
class PairwiseFaceDataLayer : public FaceDataLayer<Dtype>
{
public: //--------CTOR & DTOR--------//
	// Default constructor and destructor
	explicit PairwiseFaceDataLayer(const caffe::LayerParameter& param);
	virtual ~PairwiseFaceDataLayer() = default;

public: //--------CAFFE INTERFACES--------//
	// Be called only once when the network initializing
	//	[DataLayerSetUp]
	//		├[_LoadLandmarks]
	//		└[_LoadPairs]
	virtual void DataLayerSetUp(
			const std::vector<caffe::Blob<Dtype>*>& bottom,
			const std::vector<caffe::Blob<Dtype>*>& top
			);
	virtual void Reshape(
			const vector<Blob<Dtype>*>& bottom,
    		const vector<Blob<Dtype>*>& top
    		);
	// Be called on every forwardings
	//	[load_batch]
	//		├[_LoadPatches]
	//		│	├[_ImageAugmentation]
	//		│	└[_GetFacePartRect]
	//		└[_PatchesToBuffer]
	virtual void load_batch(caffe::Batch<Dtype>* pBatch);

protected: //--------WORKING FUNCTIONS--------//
	// Check layer params and load internal used variables
	virtual void _CheckAndLoadParams();

	// Loads m_ImgPairs from the specified file of image pairs
	virtual void _LoadPairs(
			const std::string &strPairFn,
			std::vector<IMGPAIR> &imgPairs,
			int nFnCnt
			) const;

	// Loads an image by specified id and extract patches from the image
	virtual void _LoadPatches(
			const FACEDATA &data,
			int nImgId,
			std::vector<cv::Mat> &patches
			) const;
			
	virtual Dtype _LoadLabel(const FACEDATA &data, int nImgId);

protected: //--------PAIRWISEDATA MEMBERS--------//
	// All image filenames found in landmark file
	static std::vector<IMGPAIR>	*m_pTrainPairs;
	static std::vector<IMGPAIR>	*m_pTestPairs;
	
	PairwiseFaceDataParameter	m_PairParams;
};

} // namespace caffe

//	┌─┐	┌┬┐	│├┼┤ Funny huh?
//	└─┘	└┴┘	├┼┤│ Hmm...

#endif /* PAIRWISE_FACES_DATA_LAYER_HPP_ */
