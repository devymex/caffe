
//*****************************************************************************
// Exquisitely, Rigorously, Naturally.
//													-- yumengwang@deepglint.com
//*****************************************************************************


#include "caffe/layers/face_data_layer.hpp"
#include "caffe/util/img_aug.hpp"
#include "caffe/util/cvhelper.hpp"

#include <stdio.h>

#include <set>
#include <sstream>
#include <thread>

namespace caffe {

cv::Size GetImageSizeFromFile(const std::string& fileName)
{
	std::ostringstream ssCmd;
	ssCmd << "identify -format \'%w %h\' \"" << fileName << "\"";
	FILE *pipe = popen(ssCmd.str().c_str(), "r");

	cv::Size ret(-1, -1);
	if (NULL != pipe)
	{
		char szPath[64];
		if (NULL != fgets(szPath, sizeof(szPath) - 1, pipe))
		{
			std::istringstream(szPath) >> ret.width >> ret.height;
		}
		pclose(pipe);
	}
	return ret;
}

//-----------------------------------------------------------------------------
//CONSTRUCTOR & DESTRUCTOR
template <typename Dtype>
FACEDATA *FaceDataLayer<Dtype>::m_pTrainData = 0;

template <typename Dtype>
FACEDATA *FaceDataLayer<Dtype>::m_pTestData = 0;

template <typename Dtype>
FaceDataLayer<Dtype>::FaceDataLayer(const LayerParameter& param)
	: BasePrefetchingDataLayer<Dtype>(param)
	, m_rg(std::random_device("/dev/random")())
	, m_nTrainCnt(0)
	, m_nTestCnt(0)
{
	// Retrieve parameters
	_CheckAndLoadParams();
}

template <typename Dtype>
FaceDataLayer<Dtype>::~FaceDataLayer()
{
	this->StopInternalThread();
}

//-----------------------------------------------------------------------------
//INITIALIZATION
template <typename Dtype>
void FaceDataLayer<Dtype>::DataLayerSetUp(
		const std::vector<Blob<Dtype>*>& bottom,
		const std::vector<Blob<Dtype>*>& top
		)
{
	LOG(INFO) << "RANK " << caffe::Caffe::solver_rank()
		 	  << " is loading data ...";
	FACEDATA *&pData = this->phase_ == caffe::TRAIN
		? m_pTrainData
		: m_pTestData;
	if (caffe::Caffe::solver_rank() == 0)
	{
		CHECK_EQ(pData, (FACEDATA*)NULL);
		pData = new FACEDATA;
		_LoadLandmarks(
			m_Params.landmarks_file(),
			pData->imgFns,
			pData->labels,
			pData->landmarks
			);
		pData->nCursor = 0;
	}
	else
	{
		CHECK_NE(pData, (FACEDATA*)NULL);
	}
	LOG(INFO) << "Loaded " << pData->imgFns.size() << " filenames and "
			  << "landmarks from \'" << m_Params.landmarks_file() << "\'";
	CHECK_EQ(pData->imgFns.size(), pData->landmarks.size());
	if (this->phase_ == TRAIN)
	{
		m_nTrainCnt = pData->imgFns.size();
	}
	else
	{
		m_nTestCnt = pData->imgFns.size();
	}
}

//-----------------------------------------------------------------------------
//RESHAPE TOP BLOBS
template <typename Dtype>
void FaceDataLayer<Dtype>::Reshape(
			const vector<Blob<Dtype>*>& bottom,
    		const vector<Blob<Dtype>*>& top
    		)
{
	CHECK(top.size() == 2);

	// The channel is calucate by: 6 parts * 3 rgb = 18 channels
	top[0]->Reshape({
		(int)m_Params.batch_size(),
		(int)m_Params.face_parts_size() * IMAGE_CHANNELS,
		(int)m_Params.patch_crop_height(),
		(int)m_Params.patch_crop_width()
		});
	CHECK(top[0]->count() > 0);

	top[1]->Reshape({(int)m_Params.batch_size()});
	for (int i = 0; i < (int)this->prefetch_.size(); ++i)
	{
	    this->prefetch_[i]->data_.Reshape(top[0]->shape());
	    this->prefetch_[i]->label_.Reshape(top[1]->shape());
	}
}

//-----------------------------------------------------------------------------
//LOADING BATCH FOR FOWARDING
template <typename Dtype>
void FaceDataLayer<Dtype>::load_batch(Batch<Dtype>* pBatch)
{
	FACEDATA &data = this->phase_ == TRAIN ? *m_pTrainData : *m_pTestData;
	int nImgCnt = this->phase_ == TRAIN ? m_nTrainCnt : m_nTestCnt;

	int nBatchSize = m_Params.batch_size();
	Dtype *pImgBuf = pBatch->data_.mutable_cpu_data();
	Dtype *pLabelBuf = pBatch->label_.mutable_cpu_data();

	std::unique_lock<std::mutex> cursorLock(data.cvMutex);
	int nCursor = data.nCursor;
	data.nCursor = (data.nCursor + nBatchSize) % nImgCnt;
	cursorLock.unlock();
	
	int32_t nBufElems = pBatch->data_.count() / pBatch->data_.num();
	for (int b = 0; b < nBatchSize; ++b)
	{
		int nImgIdx = (b + nCursor) % nImgCnt;
		std::vector<cv::Mat> patches;
		_LoadPatches(data, nImgIdx, patches);
		for (int c = 0; c < (int)patches.size(); ++c)
		{
			Dtype *pChBuf = pImgBuf + pBatch->data_.offset(b, c * IMAGE_CHANNELS);
			_PatchesToBatchBuffer(patches[c], pChBuf, nBufElems);
		}
		pLabelBuf[pBatch->label_.offset(b)] = _LoadLabel(data, nImgIdx);
	}
}

//-----------------------------------------------------------------------------
// CHECKING AND LOADING PARAMS
template <typename Dtype>
void FaceDataLayer<Dtype>::_CheckAndLoadParams()
{
	m_Params = this->layer_param().face_data_param();
	// Check parameters
	CHECK(m_Params.shuffle_method() < SM_GUARD);
	for (int i = 0; i < m_Params.face_parts_size(); ++i)
	{
		CHECK(m_Params.face_parts(i) < FP_GUARD);
	}
	auto CHECKPROB = [](float fProb)
	{
		CHECK(fProb >= 0.0f && fProb <= 1.0f);
	};
	CHECKPROB(m_Params.illum_trans_prob());
	CHECKPROB(m_Params.gauss_blur_prob());
	CHECKPROB(m_Params.motion_blur_prob());
	CHECKPROB(m_Params.jpeg_comp_prob());
	CHECKPROB(m_Params.res_change_prob());
	CHECKPROB(m_Params.hsv_adjust_prob());

	// Retrieve parent path of images
	m_strImgPath = m_Params.img_path();
	if (!m_strImgPath.empty() && m_strImgPath.back() != '/')
	{
		m_strImgPath.push_back('/');
	}

	// cv::Size assignments
	m_FaceFullSize.width =		(float)m_Params.face_full_width();
	m_FaceFullSize.height =		(float)m_Params.face_full_height();
	m_FaceRimlessSize.width =	(float)m_Params.face_rimless_width();
	m_FaceRimlessSize.height =	(float)m_Params.face_rimless_height();
	m_FacePartSize.width =		(float)m_Params.face_part_width();
	m_FacePartSize.height =		(float)m_Params.face_part_height();
	m_PatchCropSize.width =		(float)m_Params.patch_crop_width();
	m_PatchCropSize.height =	(float)m_Params.patch_crop_height();
	m_PatchFullSize.width =		(float)m_Params.patch_full_width();
	m_PatchFullSize.height =	(float)m_Params.patch_full_height();
}

//-----------------------------------------------------------------------------
//LOADING LANDMARKS FROM FILE
template <typename Dtype>
void FaceDataLayer<Dtype>::_LoadLandmarks(
			const std::string &strLmFn,
			std::vector<std::string> &imgFns,
			std::vector<int> &labels,
			std::vector<LANDMARK> &landmarks
			) const
{
	//temporary variables
	std::set<std::string> fnSet; // for verify the uniqueness of filenames

	//Load the landmark file
	std::ifstream inFile(strLmFn);
	LOG_ASSERT(inFile.is_open());
	int nLandmarks = 0;
	for (std::string strLine; std::getline(inFile, strLine); ++nLandmarks)
	{
		if (strLine.empty())
		{
			continue;
		}
		// Read filename of image from one line.
		std::istringstream iss(strLine);
		std::string strImgFn;
		int nLabel;
		if (iss >> strImgFn >> nLabel)
		{
			// Verify the filename is unseen
			if (fnSet.find(strImgFn) != fnSet.end())
			{
				LOG(FATAL) << "strImgFn=" << strImgFn;
				exit(-1);
			}
			fnSet.insert(strImgFn);

			std::string strFullFn = m_strImgPath + strImgFn;
			//cv::Size imgSize = GetImageSizeFromFile(strFullFn);
			cv::Size imgSize(10000, 10000);
			if (imgSize.width <= 0 || imgSize.height <= 0)
			{
				LOG(FATAL) << "strFullFn=" << strFullFn;
				exit(-1);
			}
			// Read landmarks
			LANDMARK lm;
			for (cv::Point2f pt; iss >> pt.x >> pt.y;)
			{
				if (pt.x < 0 || pt.x > imgSize.width ||
					pt.y < 0 || pt.y > imgSize.height)
				{
					break;
				}
				lm.push_back(pt);
			}
			if (lm.size() != LANDMARK_LENGTH)
			{
				LOG(FATAL) << "strFullFn=" << strFullFn;
				exit(-1);
			}
			// Add the landmark into array
			imgFns.push_back(strImgFn);
			labels.push_back(nLabel);
			landmarks.push_back(lm);
		}
		if (nLandmarks % 100000 == 0 && nLandmarks > 0)
		{
			LOG(INFO) << "nLandmarks=" << nLandmarks;
		}
	}
}

//-----------------------------------------------------------------------------
//LOADING IMAGE PATCHS
template <typename Dtype>
void FaceDataLayer<Dtype>::_LoadPatches(
		const FACEDATA &data,
		int nImgId,
		std::vector<cv::Mat> &patches
		) const
{
	const LANDMARK &lm = data.landmarks[nImgId];
	cv::Mat img = cv::imread(m_strImgPath + data.imgFns[nImgId]);
	CHECK(!img.empty());

	//Do data augmentation
	std::uniform_int_distribution<> rAugM(0, AM_GUARD - 1);
	AUG_METHOD augMethod = (AUG_METHOD)rAugM(m_rg);
	if (!_ImageAugmentation(img, augMethod))
	{
		augMethod = (AUG_METHOD)-1;
	}

	cv::Size border = m_PatchFullSize - m_PatchCropSize;
	std::uniform_int_distribution<> rWidth(0, border.width - 1);
	std::uniform_int_distribution<> rHeight(0, border.height - 1);
	for (int i = 0; i < m_Params.face_parts_size(); ++i)
	{
		FACE_PART iPart = (FACE_PART)m_Params.face_parts(i);
		cv::Rect2f partRect(_GetFacePartRect(img.size(), lm, iPart));
		// srcRect is the intersection of image rect and partRect
		cv::Rect2f srcRect(cv::Rect2f(cv::Point2f(), img.size()) & partRect);
		// Transform the coordination system of srcRect from image to part
		cv::Rect2f dstRect(srcRect.tl() - partRect.tl(), srcRect.size());

		// build a part image with black background
		cv::Mat part = cv::Mat::zeros(partRect.size(), img.type());
		// Take the copy from image and do resize
		img(srcRect).copyTo(part(dstRect));
		cv::resize(part, part, m_PatchFullSize);

		// Make crop of the patch
		cv::Point cropTL = m_Params.patch_rand_crop() ?
			cv::Point(rWidth(m_rg), rHeight(m_rg)) : cv::Point(border / 2);
		// Add the patch of cropsize into results
		patches.push_back(part(cv::Rect(cropTL, m_PatchCropSize)).clone());
	}
}

//-----------------------------------------------------------------------------
//LOADING IMAGE PATCHS
template <typename Dtype>
Dtype FaceDataLayer<Dtype>::_LoadLabel(const FACEDATA &data, int nImgId)
{
	return data.labels[nImgId];
}

//-----------------------------------------------------------------------------
//LOADING IMAGE PATCHS
template <typename Dtype>
bool FaceDataLayer<Dtype>::_ImageAugmentation(
	cv::Mat &img,
	AUG_METHOD augMethod
	) const
{
	CHECK_EQ(img.type(), CV_8UC3);
	CHECK_LT(augMethod, AM_GUARD);

	auto UNIPROBTEST = [](float fProb) -> bool
	{
		return (std::rand() / (RAND_MAX + 1.0f)) < fProb;
	};

	switch(augMethod)
	{
	case AM_ILLUMIOUS:
		if (UNIPROBTEST(m_Params.illum_trans_prob()))
		{
			std::uniform_int_distribution<> rMean(50, 200);
			int nNewMean = rMean(m_rg);
			ImgIllumTrans(img, nNewMean);
			return true;
		}
		break;
	case AM_GAUSS_BLUR:
		if (UNIPROBTEST(m_Params.gauss_blur_prob()))
		{
			std::uniform_int_distribution<> rSize(1, 2);
			int nKernelSize = rSize(m_rg) * 2 + 1; // must be 3 or 5
			cv::GaussianBlur(img, img, cv::Size(nKernelSize, nKernelSize), 0, 0);
			return true;
		}
		break;
	case AM_MOTION_BLUR:
		if (UNIPROBTEST(m_Params.motion_blur_prob()))
		{
			// NEEDS REVIEW!
			const int sz = 30;
			std::uniform_int_distribution<> rSize(0, sz - 3);
			std::uniform_real_distribution<float> rTheta(-(float)M_PI, (float)M_PI);
			cv::Mat kernel = GetMotionKernel(sz, rSize(m_rg) + 1, rTheta(m_rg));
			cv::filter2D(img, img, img.depth(), kernel);
			return true;
		}
		break;
	case AM_JPEG_COMP:
		if (UNIPROBTEST(m_Params.jpeg_comp_prob()))
		{
			std::uniform_int_distribution<> rQuality(30, 60);
			ImgJpegComp(img, rQuality(m_rg));
			return true;
		}
		break;
	case AM_RESOLUTION:
		if (UNIPROBTEST(m_Params.res_change_prob()))
		{
			std::uniform_real_distribution<float> rScale(0.2f, 1.0f);
			ImgResChange(img, rScale(m_rg));
			return true;
		}
		break;
	case AM_HSV_ADJ:
		if (UNIPROBTEST(m_Params.hsv_adjust_prob()))
		{
			std::uniform_int_distribution<> rh(-20, 20);
			std::uniform_int_distribution<> rs(-40, 40);
			ImgHsvAdjust(img, rh(m_rg), rs(m_rg), 0);
			return true;
		}
		break;
	default:
		break;
	}
	return false;
}

template <typename Dtype>
cv::Rect2f FaceDataLayer<Dtype>::_GetFacePartRect(
		const cv::Size &imgSize,
		const LANDMARK &lm,
		FACE_PART facePart
		) const
{
	CHECK_LT(facePart, FP_GUARD);
	switch (facePart)
	{
	case FP_FULL_FACE:
		return RectOfCenter(CenterOfSize(cv::Size2f(imgSize)), m_FaceFullSize);
	case FP_LEFT_EYE:
		return RectOfCenter(lm[21], m_PatchCropSize);
	case FP_RIGHT_EYE:
		return RectOfCenter(lm[38], m_PatchCropSize);
	case FP_NOSE:
		return RectOfCenter(lm[57], m_PatchCropSize);
	case FP_MOUTH:
		return RectOfCenter(MiddelPoint(lm[58], lm[62]), m_PatchCropSize);
	case FP_MIDDLE_EYE:
		return RectOfCenter(MiddelPoint(lm[21], lm[38]), m_PatchCropSize);
	case FP_LEFT_MOUTH:
		return RectOfCenter(lm[58], m_PatchCropSize);
	case FP_RIGHT_MOUTH:
		return RectOfCenter(lm[62], m_PatchCropSize);
	case FP_RIMLESS_FACE:
		return RectOfCenter(CenterOfSize(cv::Size2f(imgSize)), m_FaceRimlessSize);
	case FP_LEFT_BROW:
		return RectOfCenter(lm[24], m_PatchCropSize);
	case FP_RIGHT_BROW:
		return RectOfCenter(lm[41], m_PatchCropSize);
	case FP_MIDDLE_BROW:
		return RectOfCenter(MiddelPoint(lm[24], lm[41]), m_PatchCropSize);
	default:
		return cv::Rect2f();
	}
}

template <typename Dtype>
void FaceDataLayer<Dtype>::_PatchesToBatchBuffer(
		const cv::Mat &patch,
		Dtype *pBuf,
		uint32_t nBufElems
		) const
{
	CHECK_EQ(CV_MAT_DEPTH(patch.type()), CV_8U);
	CHECK_LE(patch.size().area() * patch.channels(), nBufElems);

	cv::Size imgSize = patch.size();
	int nImgArea = imgSize.area();
	int nDepth = cv::DataDepth<Dtype>::value;

	cv::Mat fltImg;
	patch.convertTo(fltImg, nDepth, (Dtype)(1.0 / 255.0), 0);

	std::vector<cv::Mat> channels;
	for (int i = 0; i < patch.channels(); ++i)
	{
		channels.push_back(cv::Mat(imgSize, nDepth, pBuf + i * nImgArea));
	}
	cv::split(fltImg, channels);
}

INSTANTIATE_CLASS(FaceDataLayer);
REGISTER_LAYER_CLASS(FaceData);

} // namespace caffe
