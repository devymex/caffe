#include "caffe/layers/pairwise_face_loss_layer.hpp"
#include "caffe/layer.hpp"

#include <time.h>

#include <algorithm>
#include <iostream>
#include <vector>

namespace caffe {

#define RED "\e[91m"
#define CLEAR "\e[0m"

const float g_fEpsilon = 1e-8;

template <typename Dtype>
PairwiseFaceLossLayer<Dtype>::PairwiseFaceLossLayer(const LayerParameter& param)
			: LossLayer<Dtype>(param)
{
	m_Params = param.pairwise_face_loss_param();
}

template <typename Dtype>
void PairwiseFaceLossLayer<Dtype>::LayerSetUp(
	const vector<Blob<Dtype>*>& bottom,
	const vector<Blob<Dtype>*>& top
	)
{
	CHECK_EQ(bottom.size(), 3)
		<< RED "Bottoms must be two featuers and their label!" CLEAR;
	CHECK_EQ(top.size(), 1)
		<< RED "Top must be only value for loss!" CLEAR;
	CHECK_EQ(bottom[0]->count(), bottom[1]->count())
		<< RED "Two features has different size!" CLEAR;
	CHECK_NE(bottom[0]->count(), 0)
		<< RED "Feature is empty!" CLEAR;
	CHECK_EQ(bottom[0]->num(), bottom[2]->count())
		<< RED "Batch size of features does not equal to their label!" CLEAR;
}

template <typename Dtype>
void PairwiseFaceLossLayer<Dtype>::Reshape(
	const std::vector<Blob<Dtype>*>& bottom,
	const std::vector<Blob<Dtype>*>& top
	)
{
	top[0]->Reshape({bottom[0]->num()});
}

template <typename Dtype>
void PairwiseFaceLossLayer<Dtype>::Forward_cpu(
	const std::vector<Blob<Dtype>*>& bottom,
	const std::vector<Blob<Dtype>*>& top
	)
{
	int nBatchSize = bottom[0]->num();
	int nVecLen = bottom[0]->count() / nBatchSize;
	std::vector<Dtype> diff(nVecLen);
	
	const Dtype *pFeatL = bottom[0]->cpu_data();
	const Dtype *pFeatR = bottom[1]->cpu_data();
	const Dtype *pLabel = bottom[2]->cpu_data();

	Dtype *pGradL = bottom[0]->mutable_cpu_diff();
	Dtype *pGradR = bottom[1]->mutable_cpu_diff();
	Dtype *pLoss = top[0]->mutable_cpu_data();

	caffe_set(top[0]->count(), Dtype(0), pLoss);
	for (int iBatch = 0; iBatch < nBatchSize; ++iBatch)
	{
		int nOffsetL = bottom[0]->offset(iBatch);
		int nOffsetR = bottom[1]->offset(iBatch);
		Dtype fDist = 0.0f;
		for (int iElem = 0; iElem < nVecLen; ++iElem)
		{
			diff[iElem] = pFeatL[nOffsetL + iElem] - pFeatR[nOffsetR + iElem];
			fDist += diff[iElem] * diff[iElem];
		}
		Dtype fPi = (std::abs(pLabel[iBatch]) > g_fEpsilon) ? 1.0f : -1.0f;
		Dtype fPow = fPi * (fDist - m_Params.theta());
		Dtype fGrad = 0.0f;
		if (fPow < 20)
		{
			Dtype fExp = m_Params.beta() * std::exp(fPow);
			if (std::abs(fExp) > g_fEpsilon)
			{
				pLoss[iBatch] = std::log(1.0f + fExp);
				fGrad = fPow / (1.0f / fExp + 1.0f);
			}
		}
		else
		{
			pLoss[iBatch] = fPow + std::log(m_Params.beta());
			fGrad = 1.0f;
		}

		pLoss[iBatch] /= nBatchSize;
		fGrad = (fGrad * 2.0f) / nBatchSize;

		for (int iElem = 0; iElem < nVecLen; ++iElem)
		{
			pGradL[nOffsetL + iElem] = fGrad * diff[iElem];
			pGradR[nOffsetR + iElem] = fGrad * -diff[iElem];
		}
	}
}

template <typename Dtype>
void PairwiseFaceLossLayer<Dtype>::Backward_cpu(
		const std::vector<Blob<Dtype>*>& top,
		const std::vector<bool>& propagate_down,
		const std::vector<Blob<Dtype>*>& bottom
	)
{
	if (!propagate_down[0])
	{
		caffe_set(bottom[0]->count(), Dtype(0), bottom[0]->mutable_cpu_diff());
	}
	if (!propagate_down[1])
	{
		caffe_set(bottom[1]->count(), Dtype(0), bottom[1]->mutable_cpu_diff());
	}
	if (propagate_down[2])
	{
		//CHECK_EQ(propagate_down[2], false)
		//	<< RED "Gradient of label never propagate backward!" CLEAR;
	}
}

#ifdef CPU_ONLY
STUB_GPU(PairwiseFaceLossLayer);
#endif

INSTANTIATE_CLASS(PairwiseFaceLossLayer);
REGISTER_LAYER_CLASS(PairwiseFaceLoss);

}  // namespace caffe
