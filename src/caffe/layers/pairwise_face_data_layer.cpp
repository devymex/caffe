
//*****************************************************************************
// Exquisitely, Rigorously, Naturally.
//													-- yumengwang@deepglint.com
//*****************************************************************************


#include "caffe/layers/pairwise_face_data_layer.hpp"
#include "caffe/util/img_aug.hpp"
#include "caffe/util/cvhelper.hpp"

#include <stdio.h>

#include <set>
#include <sstream>
#include <thread>

namespace caffe {

//-----------------------------------------------------------------------------
//CONSTRUCTOR & DESTRUCTOR
template <typename Dtype>
std::vector<IMGPAIR> *PairwiseFaceDataLayer<Dtype>::m_pTrainPairs = 0;

template <typename Dtype>
std::vector<IMGPAIR> *PairwiseFaceDataLayer<Dtype>::m_pTestPairs = 0;

template <typename Dtype>
PairwiseFaceDataLayer<Dtype>::PairwiseFaceDataLayer(const LayerParameter& param)
	: FaceDataLayer<Dtype>(param)
{
	// Retrieve parameters
	_CheckAndLoadParams();
}

//-----------------------------------------------------------------------------
//INITIALIZATION
template <typename Dtype>
void PairwiseFaceDataLayer<Dtype>::DataLayerSetUp(
		const std::vector<caffe::Blob<Dtype>*>& bottom,
		const std::vector<caffe::Blob<Dtype>*>& top
		)
{
	FaceDataLayer<Dtype>::DataLayerSetUp(bottom, top);
	
	bool bIsTrain = this->phase_ == TRAIN;
	std::vector<IMGPAIR> *&pPairs = bIsTrain ? m_pTrainPairs : m_pTestPairs;
	int nImgCnt = bIsTrain ? this->m_nTrainCnt : this->m_nTestCnt;
	if (Caffe::solver_rank() == 0)
	{
		CHECK_EQ(pPairs, (std::vector<IMGPAIR>*)NULL);
		pPairs = new std::vector<IMGPAIR>;
		_LoadPairs(
			m_PairParams.imgpairs_file(),
			*pPairs,
			nImgCnt
			);
		LOG(INFO) << "Loaded " << pPairs->size() << " train pairs "
				<< "and labels from \'"<< m_PairParams.imgpairs_file() << "\'";
	}
	else
	{
		CHECK_NE(pPairs, (std::vector<IMGPAIR>*)NULL);
	}
	if (this->phase_ == TRAIN)
	{
		this->m_nTrainCnt = pPairs->size();
	}
	else
	{
		this->m_nTestCnt = pPairs->size();
	}
}

//-----------------------------------------------------------------------------
//RESHAPE TOP BLOBS
template <typename Dtype>
void FaceDataLayer<Dtype>::Reshape(
			const vector<Blob<Dtype>*>& bottom,
    		const vector<Blob<Dtype>*>& top
    		)
{
	CHECK(top.size() == 2);

	// The channel is calucate by: 2 images * 6 parts * 3 rgb = 36 channels
	top[0]->Reshape({
		(int)m_Params.batch_size(),
		(int)m_Params.face_parts_size() * IMAGE_CHANNELS * 2,
		(int)m_Params.patch_crop_height(),
		(int)m_Params.patch_crop_width()
		});
	CHECK(top[0]->count() > 0);

	top[1]->Reshape({(int)m_Params.batch_size()});
	for (int i = 0; i < (int)this->prefetch_.size(); ++i)
	{
	    this->prefetch_[i]->data_.Reshape(top[0]->shape());
	    this->prefetch_[i]->label_.Reshape(top[1]->shape());
	}
}

//-----------------------------------------------------------------------------
//LOADING BATCH FOR FOWARDING
template <typename Dtype>
void PairwiseFaceDataLayer<Dtype>::load_batch(caffe::Batch<Dtype>* pBatch)
{
	FaceDataLayer<Dtype>::load_batch(pBatch);
}

//-----------------------------------------------------------------------------
// CHECKING AND LOADING PARAMS
template <typename Dtype>
void PairwiseFaceDataLayer<Dtype>::_CheckAndLoadParams()
{
	FaceDataLayer<Dtype>::_CheckAndLoadParams();
	m_PairParams = this->layer_param().pairwise_face_data_param();
}

//-----------------------------------------------------------------------------
//LOADING IMAGE PAIRS FROM FILE
template <typename Dtype>
void PairwiseFaceDataLayer<Dtype>::_LoadPairs(
		const std::string &strPairFn,
		std::vector<IMGPAIR> &imgPairs,
		int nFnCnt
		) const
{
	imgPairs.clear();
	std::ifstream inFile(strPairFn);
	int nPairs = 0;
	for (std::string strLine; std::getline(inFile, strLine); ++nPairs)
	{
		std::istringstream iss(strLine);
		int nImgId1, nImgId2, nLabel;
		if (iss >> nImgId1 >> nImgId2 >> nLabel)
		{
			if (nLabel != 0 && nLabel != 1)
			{
				LOG(WARNING) << "nLabel=" << nLabel;
				continue;
			}
			if (nImgId1 >= nFnCnt)
			{
				LOG(WARNING) << "nImgId1=" << nImgId1;
				continue;
			}
			if (nImgId2 >= nFnCnt)
			{
				LOG(WARNING) << "nImgId2=" << nImgId2;
				continue;
			}
			imgPairs.push_back({nImgId1, nImgId2, nLabel});
		}
		if (nPairs % 10000000 == 0 && nPairs > 0)
		{
			LOG(INFO) << "nPairs=" << nPairs;
		}
	}
}

//-----------------------------------------------------------------------------
//LOADING IMAGE PATCHS
template <typename Dtype>
void PairwiseFaceDataLayer<Dtype>::_LoadPatches(
		const FACEDATA &data,
		int nImgId,
		std::vector<cv::Mat> &patches
		) const
{
	const std::vector<IMGPAIR> &pairs = this->phase_ == TRAIN ?
		*m_pTrainPairs : *m_pTestPairs;
	FaceDataLayer<Dtype>::_LoadPatches(data, pairs[nImgId].imgId1, patches);
	FaceDataLayer<Dtype>::_LoadPatches(data, pairs[nImgId].imgId2, patches);
}

template <typename Dtype>
Dtype PairwiseFaceDataLayer<Dtype>::_LoadLabel(const FACEDATA &data, int nImgId)
{
	const std::vector<IMGPAIR> &pairs = this->phase_ == caffe::TRAIN
		? *m_pTrainPairs
		: *m_pTestPairs;
	return pairs[nImgId].label;
}

INSTANTIATE_CLASS(PairwiseFaceDataLayer);
REGISTER_LAYER_CLASS(PairwiseFaceData);

} // namespace caffe
